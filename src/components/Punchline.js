import React, { Component } from 'react';
import '../App.css';


class Punchline extends Component { 
  render() {
    return (
      <div className="Punchline">
        <h1>Quel MC a dit ?</h1> 
         <p>{this.props.punch.content}</p> 
      </div>
    );
  }
}

export default Punchline;