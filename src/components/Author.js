import React, { Component } from 'react';
import '../App.css';


class Author extends Component {
  render() {
    return (
      <div 
        className="Author" 
        onClick={() => this.props.selectAuthor(this.props.author.id)} 
        style={{background: this.props.color}}
      >
        <p>{this.props.author.last_name}</p>
      </div>
    );
  }
}

export default Author;