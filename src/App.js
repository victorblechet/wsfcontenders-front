import React, { Component } from 'react';
import Punchline from './components/Punchline';
import './App.css';
import Author from './components/Author';



class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      authors: null,
      punchline: null,
      selectedAuthor: null,
      goodAnswer: 0,
    }
  }

  apicall(){
    fetch('http://127.0.0.1:8000/query', {
      method: 'POST',
      headers: { 'Content-Type': 'text/plain' },
      body: JSON.stringify({ query: '{getQuizz{punchline{id content author_id} authors{id last_name first_name surname}}}' })
    })
    .then(res => res.json())
    .then(res => this.setState({authors: shuffle(res.data.getQuizz.authors), punchline: res.data.getQuizz.punchline, selectedAuthor: null}));
  }

  componentDidMount() {
    this.apicall();
  }


  componentWillUpdate() {
    if (this.state.punchline === null) {
      return;
    }
    if (this.state.selectedAuthor === this.state.punchline.author_id){
      this.setState({goodAnswer: this.state.goodAnswer + 1});
      this.apicall();
      return;
    } 
  }
  
  setColor(id){
    if (this.state.selectedAuthor === id){
      return "green"
    }
    return ""; 
  }

  render() {
    if (this.state.authors == null || this.state.punchline == null) {
      return (<div></div>)
    }

    return (
      <div className="App">
          <Punchline punch={this.state.punchline}/>
          {this.state.authors.map(author =>
            <Author 
              author={author} 
              selectAuthor={
                (key) => this.setState({selectedAuthor: key},  
              )} 
              key={author.id} 
              color={this.setColor(author.id)}
            />  
          )}
      </div>
    );
  }
}



function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
      // Pick a random index
      let index = Math.floor(Math.random() * counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      let temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
  }

  return array;
}




export default App;
